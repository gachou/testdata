# [0.12.0](https://gitlab.com/gachou/testdata/compare/v0.11.3...v0.12.0) (2020-11-15)

### Features

- add image with "orientation"-tag
  ([f00b797](https://gitlab.com/gachou/testdata/commit/f00b79718b56e2649c890fcaf8e274a814fd438a))

## [0.11.3](https://gitlab.com/gachou/testdata/compare/v0.11.2...v0.11.3) (2020-11-10)

### Bug Fixes

- check for validity before acquiring lock
  ([3b06de1](https://gitlab.com/gachou/testdata/commit/3b06de199bbba5f171de7a8816b1cd95471e2cda))

## [0.11.2](https://gitlab.com/gachou/testdata/compare/v0.11.1...v0.11.2) (2020-11-06)

### Bug Fixes

- more retries with shorter delays when waiting for lock
  ([3ef887e](https://gitlab.com/gachou/testdata/commit/3ef887edf4f963642d5133496bbb7f83f313bcd1))

## [0.11.1](https://gitlab.com/gachou/testdata/compare/v0.11.0...v0.11.1) (2020-11-06)

### Bug Fixes

- use lockfile to prevent concurrent downloads
  ([3da44c0](https://gitlab.com/gachou/testdata/commit/3da44c09647463b6521dfd742e0bfda228c26003))

# [0.11.0](https://gitlab.com/gachou/testdata/compare/v0.10.1...v0.11.0) (2020-11-05)

### Features

- create backup of files with invalid checksum, to allow later
  inspection
  ([520c39a](https://gitlab.com/gachou/testdata/commit/520c39a19ca05ceb1fd376f4561812b8728a079c))

## [0.10.1](https://gitlab.com/gachou/testdata/compare/v0.10.0...v0.10.1) (2020-11-01)

### Bug Fixes

- adjust signatur of "tempdir.resolve" to use rest-parameters
  ([3992be1](https://gitlab.com/gachou/testdata/commit/3992be1aba918542e39f1619010b5531a8747172))

# [0.10.0](https://gitlab.com/gachou/testdata/compare/v0.9.0...v0.10.0) (2020-11-01)

### Features

- remove cuid-usage from tempdir and fixtures
  ([dc96dd2](https://gitlab.com/gachou/testdata/commit/dc96dd2a2fbfcbb80d5279a3a073d2a2cc290102))

# [0.9.0](https://gitlab.com/gachou/testdata/compare/v0.8.0...v0.9.0) (2020-10-31)

### Features

- concurrency-safe methods for load-fixture and tempDir
  ([e287246](https://gitlab.com/gachou/testdata/commit/e28724627573ffc50850706695b0caf6dcca8506))

# [0.8.0](https://gitlab.com/gachou/testdata/compare/v0.7.0...v0.8.0) (2020-10-26)

# [0.7.0](https://gitlab.com/gachou/testdata/compare/v0.5.1...v0.7.0) (2020-10-26)

### Bug Fixes

- adjust file-paths on site to expected paths in code
  ([57d569d](https://gitlab.com/gachou/testdata/commit/57d569d5dd4a5aa2da50e97d08493dd29fa58012))
- fix examples, generate api docs
  ([2736251](https://gitlab.com/gachou/testdata/commit/273625101ad094ec9a7de5d6f0585bae46ea78c4))
- **download:** use trailing slash in rsync-command
  ([9acd001](https://gitlab.com/gachou/testdata/commit/9acd0018b886ad17eecd1134101c9b80652287ba))
- **upload:** testDataPath is a function now
  ([7d39f5f](https://gitlab.com/gachou/testdata/commit/7d39f5f8490250b5969b587048960ecd0c8ee563))

### Features

- add AVI file
  ([dbf437d](https://gitlab.com/gachou/testdata/commit/dbf437d3d7396db5b4c348a3d49c0497cba0bc60))
