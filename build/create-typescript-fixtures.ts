import globby from "globby";
import path from "path";
import { removeTrailingPipes } from "../src/internal/remove-trailing-pipes";
import { computeHash } from "../src/internal/validate-file";

interface File {
	name: string;
	hash: string;
}

export async function createTypescriptFixtures(basePath: string): Promise<string> {
	const files = await globby("**", { cwd: basePath });
	const records = await asTypeScriptRecord(files, basePath);
	return removeTrailingPipes`
      |/* eslint-disable */
      |// prettier-ignore
      |export interface TestDataFile {
      |   name: string;
      |   hash: string;
      |}
      |export interface Fixtures {
      |   ${createFixturesFields(files)}
      |};
      |
      |export const files: Fixtures = ${records};
  `;
}

function createFixturesFields(files: string[]) {
	return files
		.map((fileName) => {
			const propertyName = JSON.stringify(fileName);
			return removeTrailingPipes`
          |    ${propertyName}: TestDataFile;
      `;
		})
		.join("\n");
}

async function asTypeScriptRecord(files: string[], basePath: string) {
	const fileEntries: File[] = await Promise.all(
		files.map(
			async (file: string): Promise<File> => ({
				name: file,
				hash: await computeHash(path.join(basePath, file)),
			})
		)
	);

	const fileRecords = keyBy(fileEntries, (entry) => entry.name);
	return JSON.stringify(fileRecords, null, 2);
}

function keyBy<T>(list: T[], keyFn: (item: T) => string): Record<string, T> {
	const result: Record<string, T> = {};
	list.forEach((item) => {
		result[keyFn(item)] = item;
	});
	return result;
}
