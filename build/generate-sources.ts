import { createTypescriptFixtures } from "./create-typescript-fixtures";
import fs from "fs-extra";
import path from "path";
import prettier from "prettier";

const outputFile = path.resolve(__dirname, "..", "src", "generated", "index.ts");
const testDataPath = path.resolve(__dirname, "..", "testdata");

createTypescriptFixtures(testDataPath)
	.then(async (code) => {
		const prettyCode = await prettier.format(code, {parser: 'babel-ts'});
		await fs.mkdirp(path.dirname(outputFile));
		await fs.writeFile(outputFile, prettyCode, "utf-8");
	})
	// eslint-disable-next-line no-console
	.catch((error) => console.error(error, error.stack));
