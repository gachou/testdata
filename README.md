# @gachou/testdata

[![NPM version](https://img.shields.io/npm/v/@gachou/testdata.svg)](https://npmjs.com/package/@gachou/testdata)

> Testdata for the gachou-projects

This project provides access to the test-data used in the gachou
picture-album project.

The data itself is not part of the project, but is downlaoded
on-the-fly once it is used. It is cached in the folder that
GACHOU_TESTDATA points to.

The package contains TypeScript interfaces for access to all files, so
with an appropriate IDE you get autocompletion as well.

Hashes for every file are stored in the package in order to ensure
that the file is really what it appears to be.

# Adding files

New testfiles can be added to the "testdata"-subdirectory via PR.
Please make sure that

1. The file is as small as possible
2. You are the creator of the file and you want to distribute it as
   Public Domain (CC0)
3. The file does not contain explicit or violent content. Preferable,
   the file does not contain any humans at all.
4. You provide a good reason why your file is needed in the testdata
   set.

# Installation

Before installing the package, you have to set the environment
variable GACHOU_TESTDATA. It should point to an empty directory on
your computer.

```
export GACHOU_TESTDATA=$HOME/gachou-testdata
```

It is recommended that you set this variable on startup (e.g. in your
`$HOME/.profile` or `.bashrc`).

```
npm install @gachou/testdata
```

# Usage

The following example demonstrates how to use this module:

```js
const { loadFixture } = require("@gachou/testdata");

// This will store the file `basic/1-video-streamable.mp4` in the
loadFixture("basic/0-novideo.mp4")
  .andCopyTo("somedir/tmp.mp4")
  .then(() => console.log("done"));
```

This will generate the following output

```
done
```

# API documentation

Apidocs are here: [generated by typedoc](./apidocs)

# License

`@gachou/testdata` is published under the MIT-license.

See [LICENSE](LICENSE) for details.

# Release-Notes

For release notes, see [CHANGELOG.md](CHANGELOG.md)

# Contributing guidelines

See [CONTRIBUTING.md](CONTRIBUTING.md).
