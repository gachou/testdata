import path from "path";
import { clearCache, loadFixture, temporaryDirectoryForTest } from "../src";
import fs from "fs-extra";
import { setupNock } from "./internal/test-utils/setup-nock";
import nock from "nock";

setupNock();

const tmpDir = temporaryDirectoryForTest(module);
const responseFunction = jest.fn();
const fixtureSource = require.resolve("../testdata/basic/2-video-unstreamable.mp4");

beforeEach(async () => {
	responseFunction.mockReset().mockImplementation(replayWithFileFromTestdataDirectory);
	nock("https://gachou.gitlab.io")
		.persist()
		.get("/testdata/files/basic/2-video-unstreamable.mp4")
		.delay(500)
		.reply(200, responseFunction);
	await clearCache();
});

beforeEach(() => tmpDir.refresh());

describe("loadFixture().andCopyTo()", () => {
	it("copies the file to a specified location", async function () {
		const targetFile = tmpDir.resolve("video.mp4");
		await loadFixture("basic/2-video-unstreamable.mp4").andCopyTo(targetFile);
		expect(fs.readFileSync(targetFile)).toEqual(fs.readFileSync(fixtureSource));
	});

	it("returns the target filename", async function () {
		const targetFile = tmpDir.resolve("video.mp4");
		const result = await loadFixture("basic/2-video-unstreamable.mp4").andCopyTo(targetFile);
		expect(result).toEqual(targetFile);
	});

	it("makes only a single http-request on concurrent function calls", async function () {
		const targetFile1 = tmpDir.resolve("video1.mp4");
		const targetFile2 = tmpDir.resolve("video2.mp4");
		await Promise.all([
			loadFixture("basic/2-video-unstreamable.mp4").andCopyTo(targetFile1),
			loadFixture("basic/2-video-unstreamable.mp4").andCopyTo(targetFile2),
		]);
		expect(fs.readFileSync(targetFile1)).toEqual(fs.readFileSync(fixtureSource));
		expect(fs.readFileSync(targetFile2)).toEqual(fs.readFileSync(fixtureSource));

		expect(responseFunction).toHaveBeenCalledTimes(1);
	});
});

describe("subsequent calls to 'loadFixture'", () => {
	it("make only a single http-request on subsequent calls for the same fixture", async function () {
		const targetFile1 = tmpDir.resolve("video1.mp4");
		const targetFile2 = tmpDir.resolve("video2.mp4");

		await loadFixture("basic/2-video-unstreamable.mp4").andCopyTo(targetFile1);
		await loadFixture("basic/2-video-unstreamable.mp4").andCopyTo(targetFile2);
		expect(fs.readFileSync(targetFile1)).toEqual(fs.readFileSync(fixtureSource));
		expect(fs.readFileSync(targetFile2)).toEqual(fs.readFileSync(fixtureSource));

		expect(responseFunction).toHaveBeenCalledTimes(1);
	});

	it("make multiple requests if they result in errors", async function () {
		nock("https://gachou.gitlab.io")
			.persist()
			.get("/testdata/files/basic/2-video-unstreamable.mp4")
			.delay(100)
			.reply(404, responseFunction);
		responseFunction.mockReturnValue("Test-Error (Not found)");

		const targetFile = tmpDir.resolve("video1.mp4");

		await expect(
			loadFixture("basic/2-video-unstreamable.mp4").andCopyTo(targetFile)
		).rejects.toThrow();
		await expect(
			loadFixture("basic/2-video-unstreamable.mp4").andCopyTo(targetFile)
		).rejects.toThrow();

		expect(responseFunction).toHaveBeenCalledTimes(2);
	});
});

function replayWithFileFromTestdataDirectory(uri) {
	const relativePathInTestDataDir = uri.replace(/^\/testdata\/files/, "");
	const testDataDir = path.resolve(__dirname, "..", "testdata");
	return fs.readFile(testDataDir + relativePathInTestDataDir);
}
