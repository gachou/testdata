import hasha from "hasha";
import fs from 'fs-extra';

export class InvalidMediaFileError extends Error {
	constructor(message: string) {
		super(message);
	}
}

export async function validateFile(file: string, expectedHash: string): Promise<void> {
  const actualHash: string = await computeHash(file);
  if (actualHash !== expectedHash) {
    await createBackupForLaterInspection(file, actualHash);
		throw new InvalidMediaFileError(
			`File '${file}' has an invalid checksum (should be '${expectedHash}' but is '${actualHash}')`
		);
	}
}

export async function computeHash(file: string): Promise<string> {
	return hasha.fromFile(file, { algorithm: "sha256" });
}

async function createBackupForLaterInspection(file: string, actualHash: string) {
  await fs.copy(file, file + "-"+ actualHash);
}
