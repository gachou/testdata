import { temporaryDirectoryForTest } from "../index";
import fs from "fs-extra";
import path from "path";

describe("this temporaryDirectoryForTest-function:", () => {
	describe("refresh", () => {
		it("removes files in the temp-directory", async function () {
			const tempDir = temporaryDirectoryForTest(module);
			await tempDir.refresh();

			const file = tempDir.resolve("file1.txt");
			await fs.writeFile(file, "abc");
			expect(fs.existsSync(file)).toBe(true);

			await tempDir.refresh();
			expect(fs.existsSync(file)).toBe(false);
		});

		describe("resolve", () => {
			it("to the temp dir if no filename is given", async function () {
				const tempDir = temporaryDirectoryForTest(module);
				expect(tempDir.resolve()).toEqual(tempDir.root);
			});

			it("returns the filename inside the temp dir", async function () {
				const tempDir = temporaryDirectoryForTest(module);
				expect(tempDir.resolve("test.txt")).toEqual(path.join(tempDir.root, "test.txt"));
			});

			it("returns the filename deep inside the temp dir", async function () {
				const tempDir = temporaryDirectoryForTest(module);
				expect(tempDir.resolve("deep","inside","test.txt")).toEqual(path.join(tempDir.root, "deep","inside","test.txt"));
			});

			it("to a relative directory next to this module", () => {
			  const tempDir = temporaryDirectoryForTest(module);
			  expect(tempDir.root).toMatch("src/internal/tempDir.test.ts-gachou-testdata-tmp")
      })
		});
	});
});
