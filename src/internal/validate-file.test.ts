import { validateFile } from "./validate-file";
import path from "path";
import fs from "fs-extra";
import { temporaryDirectoryForTest } from "./tempDir";

const tmpDir = temporaryDirectoryForTest(module);
const testFileHash = "83fb12017b036309c0ff6dd779bb6ac36d61e22a93a105653164651ca2a16edc";
const testFile = tmpDir.resolve("fixture.txt")


beforeEach(async () => {
    const fixture = path.relative(".", require.resolve("./test-utils/fixture.txt"));
  await tmpDir.refresh();
  await fs.copy(fixture, testFile);
  });

test("throws when the file does not exist", async () => {
	await expect(validateFile("nonexisting-file", "hash")).rejects.toThrow(
		"ENOENT: no such file or directory, open 'nonexisting-file'"
	);
});

test("throws when the hash is invalid", async () => {
	await expect(validateFile(testFile, "hash")).rejects.toThrow(
		`File 'src/internal/validate-file.test.ts-gachou-testdata-tmp/fixture.txt' has an invalid checksum (should be 'hash' but is '${testFileHash}')`
	);
});

test("creates a backup of the file, if the hash is invalid", async () => {

	try {
    await validateFile(testFile, "hash")
  } catch {
	  // ignored
  }
  expect(await fs.readFile(testFile + "-" + testFileHash)).toEqual(await fs.readFile(testFile))

});

test("returns without exception if the checksum matches", async () => {
	await expect(validateFile(testFile, testFileHash)).resolves.toEqual(undefined);
});
