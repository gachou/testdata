import fs from "fs-extra";
import path from "path";

export interface ITempDir {
	refresh(): Promise<void>;
	resolve(...filePath: string[]): string;
	root: string;
}

export function temporaryDirectoryForTest(testModule: NodeJS.Module): ITempDir {
	const dir = testModule.filename + "-gachou-testdata-tmp";
	const relativeDir = path.relative(".", dir);
	return {
		refresh: async () => {
			await fs.mkdirs(dir);
			const entries = await fs.readdir(dir);
			await Promise.all(entries.map((entry) => fs.remove(path.join(dir, entry))));
		},
		resolve: (...filePath: string[]) => path.join(relativeDir, ...filePath),
		root: relativeDir,
	};
}
