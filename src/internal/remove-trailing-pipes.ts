export function removeTrailingPipes(
	template: TemplateStringsArray,
	...substitutions: unknown[]
): string {
	const templateParts: string[] = template.map(removeTrailingPipesFromTemplatePart);
	const result: string[] = [];
	for (let index = 0; index < substitutions.length; index++) {
		result.push(templateParts[index]);
		result.push(String(substitutions[index]));
	}
	result.push(templateParts[substitutions.length]);
	return result
		.join("")
		.replace(/^[\t ]*\n/g, "")
		.replace(/\n[\t ]*$/g, "");
}

function removeTrailingPipesFromTemplatePart(templatePart: string): string {
	return templatePart.replace(/^[\t ]*\|/gm, "");
}
