import nock from "nock";
import fs from "fs-extra";
import { createFileCache } from "./download-to-cache";
import { setupNock } from "./test-utils/setup-nock";
import { temporaryDirectoryForTest } from "./tempDir";

setupNock();

const responseFunction = jest.fn();
const testFileContents = "abc";
const testFileHash = "ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad";

const cacheDirectory = temporaryDirectoryForTest(module);
const { clearCacheDirectory, downloadToCache } = createFileCache(cacheDirectory.resolve("."));

beforeEach(async () => {
	responseFunction.mockReset().mockReturnValue(testFileContents);
	nock("https://gachou.gitlab.io")
		.persist()
		.get("/testdata/files/basic/file.txt")
		.reply(200, responseFunction);
	await clearCacheDirectory();
});

test("downloads a file to the cache", async () => {
	const targetFile = await downloadToCache({
		name: "basic/file.txt",
		hash: testFileHash,
	});
	await expect(fs.readFile(targetFile, "utf-8")).resolves.toEqual(testFileContents);
	expect(responseFunction).toHaveBeenCalledTimes(1);
});

test("returns the cached file on the second download", async () => {
	const targetFile1 = await downloadToCache({
		name: "basic/file.txt",
		hash: testFileHash,
	});
	const targetFile2 = await downloadToCache({
		name: "basic/file.txt",
		hash: testFileHash,
	});
	expect(targetFile1).toEqual(targetFile2);
	expect(responseFunction).toHaveBeenCalledTimes(1);
});

test("rejects files with invalid checksum", async () => {
	await expect(
		downloadToCache({
			name: "basic/file.txt",
			hash: "aaaaaaa",
		})
	).rejects.toThrow(
		`File '${cacheDirectory.root}/basic/file.txt' has an invalid checksum (should be 'aaaaaaa' but is '${testFileHash}')`
	);
});

test("rejects missing files", async () => {
	nock("https://gachou.gitlab.io")
		.persist()
		.get("/testdata/files/basic/file-missing.txt")
		.reply(404, "Test-Error");

	await expect(
		downloadToCache({
			name: "basic/file-missing.txt",
			hash: testFileHash,
		})
	).rejects.toThrow("Response code 404 (Not Found)");
});
