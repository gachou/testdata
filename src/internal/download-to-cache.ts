import { pipeline } from "stream";
import { promisify } from "util";
import got from "got";
import fs from "fs-extra";
import path from "path";
import { TestDataFile } from "../generated";
import { validateFile } from "./validate-file";
import lockfile from "proper-lockfile";
import createDebug from "debug";

const pipelinePromised = promisify(pipeline);

const debug = createDebug("@gachou/testdata:download-to-cache");

interface FileCache {
	downloadToCache(fixture: TestDataFile, debugLogId?: string): Promise<string>;
	clearCacheDirectory(): Promise<void>;
}

export function createFileCache(cacheDir: string): FileCache {
	async function downloadToCache(fixture: TestDataFile, debugLogId?: string): Promise<string> {
		const targetFile = path.relative(".", path.join(cacheDir, fixture.name));
		await fs.mkdirp(path.dirname(targetFile));
		await ensureFileInCache(debugLogId, targetFile, fixture);
		return targetFile;
	}

	async function clearCacheDirectory(): Promise<void> {
		await fs.remove(cacheDir);
	}

	return {
		downloadToCache,
		clearCacheDirectory,
	};
}

async function ensureFileInCache(
	debugLogId: string | undefined,
	targetFile: string,
	fixture: TestDataFile
) {
	async function isValidFileInCache() {
		try {
			await validateFile(targetFile, fixture.hash);
			debug(debugLogId + ": cache hit");
			return true;
		} catch (error) {
			debug(`${debugLogId}: need tp download "${targetFile}" again, because: `, error.message);
			return false;
		}
	}

	if (await isValidFileInCache()) {
		return;
	}

	await withFileLockDo(targetFile, debugLogId, async () => {
	  // Check again to ensure that the file is still invalid after acquiring lock
		if (await isValidFileInCache()) {
			return;
		}
		await downloadFixture(fixture, targetFile);
	});
	await validateFile(targetFile, fixture.hash);
	debug(`${debugLogId}: file valid after re-download`);
}

async function downloadFixture(fixture: TestDataFile, destination: string): Promise<void> {
	const url = `https://gachou.gitlab.io/testdata/files/${encodeURI(fixture.name)}`;
	await pipelinePromised(got.stream(url), fs.createWriteStream(destination));
}

async function withFileLockDo<T>(
	targetFile: string,
	debugLogId: string | undefined,
	callback: () => Promise<T>
): Promise<T> {
	debug(debugLogId + ": waiting for lock on " + targetFile);
	const release = await lockfile.lock(targetFile, {
		realpath: false,
		retries: { retries: 600, minTimeout: 100, maxTimeout: 200 },
	});
	try {
		debug(debugLogId + ": lock on " + targetFile + " acquired");
		return await callback();
	} finally {
		debug(debugLogId + ": releasing lock on " + targetFile);
		await release();
	}
}
