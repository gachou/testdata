import { removeTrailingPipes } from './remove-trailing-pipes';

describe('this i-function', () => {
  it('should de-indent a string', function () {
    expect(removeTrailingPipes`
      |abc
      |cde
    `).toEqual('abc\ncde');
  });

  it('replaces substitions', function () {
    const abc = '123'
    expect(removeTrailingPipes`
      |abc${abc}
      |cde
    `).toEqual('abc123\ncde');
  });

  it('replaces two substitions', function () {
    const abc = '123'
    const cde = '456'
    expect(removeTrailingPipes`
      |abc${abc}
      |cde${cde}
    `).toEqual('abc123\ncde456');
  });
});
