import { files, Fixtures, TestDataFile } from "./generated";
import fs from "fs-extra";
import { createFileCache } from "./internal/download-to-cache";
import path from "path";

// Export utility functions for creating temporary directories
export { ITempDir, temporaryDirectoryForTest } from "./internal/tempDir";

const cacheDirectory = path.resolve(__dirname, "..", "cache");
const { downloadToCache, clearCacheDirectory } = createFileCache(cacheDirectory);

type LoadFixtureOptions = { logId?: string };

export interface TestFileHandler {
	andCopyTo(...path: string[]): Promise<string>;
}

export function loadFixture(fixtureName: keyof Fixtures, options?: LoadFixtureOptions): TestFileHandler {
	const fixture: TestDataFile = files[fixtureName];
	const fileInCache = downloadToCache(fixture, options?.logId);
	return {
		async andCopyTo(...target) {
      const joinedTarget = path.join(...target);
      await fs.copy(await fileInCache, joinedTarget);
			return joinedTarget;
		},
	};
}

export async function clearCache(): Promise<void> {
	await clearCacheDirectory();
}
