**[@gachou/testdata](README.md)**

> Globals

# @gachou/testdata

## Index

### Interfaces

- [Fixtures](interfaces/fixtures.md)
- [TestDataFile](interfaces/testdatafile.md)
- [TestFileHandler](interfaces/testfilehandler.md)

### Type aliases

- [LoadFixtureOptions](README.md#loadfixtureoptions)

### Variables

- [cacheDirectory](README.md#cachedirectory)
- [clearCacheDirectory](README.md#clearcachedirectory)
- [downloadToCache](README.md#downloadtocache)

### Functions

- [clearCache](README.md#clearcache)
- [loadFixture](README.md#loadfixture)

### Object literals

- [files](README.md#files)

## Type aliases

### LoadFixtureOptions

Ƭ **LoadFixtureOptions**: { logId?: undefined \| string }

_Defined in src/index.ts:12_

#### Type declaration:

| Name     | Type                |
| -------- | ------------------- |
| `logId?` | undefined \| string |

## Variables

### cacheDirectory

• `Const` **cacheDirectory**: string = path.resolve(\_\_dirname, "..",
"cache")

_Defined in src/index.ts:9_

---

### clearCacheDirectory

• **clearCacheDirectory**: clearCacheDirectory

_Defined in src/index.ts:10_

---

### downloadToCache

• **downloadToCache**: downloadToCache

_Defined in src/index.ts:10_

## Functions

### clearCache

▸ **clearCache**(): Promise\<void>

_Defined in src/index.ts:30_

**Returns:** Promise\<void>

---

### loadFixture

▸ **loadFixture**(`fixtureName`: keyof
[Fixtures](interfaces/fixtures.md), `options?`:
[LoadFixtureOptions](README.md#loadfixtureoptions)):
[TestFileHandler](interfaces/testfilehandler.md)

_Defined in src/index.ts:18_

#### Parameters:

| Name          | Type                                               |
| ------------- | -------------------------------------------------- |
| `fixtureName` | keyof [Fixtures](interfaces/fixtures.md)           |
| `options?`    | [LoadFixtureOptions](README.md#loadfixtureoptions) |

**Returns:** [TestFileHandler](interfaces/testfilehandler.md)

## Object literals

### files

▪ `Const` **files**: object

_Defined in src/generated/index.ts:26_

#### Properties:

| Name                                      | Type   | Value                                                                                                                                             |
| ----------------------------------------- | ------ | ------------------------------------------------------------------------------------------------------------------------------------------------- |
| `basic/0-novideo.mp4`                     | object | { hash: string = "181210f8f9c779c26da1d9b2075bde0127302ee0e3fca38c9a83f5b1dd8e5d3b"; name: string = "basic/0-novideo.mp4" }                       |
| `basic/00000.MTS`                         | object | { hash: string = "5e2444a85285cfe63d74d9daadbf4786c0a73215ae275113d79ca1e3afaa0f58"; name: string = "basic/00000.MTS" }                           |
| `basic/1-video-streamable.mp4`            | object | { hash: string = "2de01f02a64e49d9108be4af1440f5b06d5b5847241f6a7b8e944faaecc4a21c"; name: string = "basic/1-video-streamable.mp4" }              |
| `basic/123.mp4`                           | object | { hash: string = "693529e2c41fe6beacb7bb4d83d07ae7cf36e794a56032b8364d16647a9b1251"; name: string = "basic/123.mp4" }                             |
| `basic/2-video-unstreamable.mp4`          | object | { hash: string = "bc883ed72f91aa96fe1218488c4ef41ec49feace795c1cb06ac71218a2a65c4d"; name: string = "basic/2-video-unstreamable.mp4" }            |
| `basic/2003-04-23__15-49-58-img_0119.jpg` | object | { hash: string = "59a4e1c4fdc24bfb81758cf8f39ef97d45819d9bd0bbc991132113ea5a4774fa"; name: string = "basic/2003-04-23\_\_15-49-58-img_0119.jpg" } |
| `basic/2015-08-19_P1010301.JPG`           | object | { hash: string = "94320866df64b3d6925ac9ece8a00e5581dedc8d65e479ba071fdd1a6f7d2ba2"; name: string = "basic/2015-08-19_P1010301.JPG" }             |
| `basic/2016-08-02__11-00-53-p1050073.jpg` | object | { hash: string = "cc9157e72132d11e69dea5e1dca748137e46315cb69dd5a0cb6fa22435fa5308"; name: string = "basic/2016-08-02\_\_11-00-53-p1050073.jpg" } |
| `basic/3-fuji-xe3.mov`                    | object | { hash: string = "44bd2ee21171ddf84dc3255bb21a3b9ef473e5983617beff656b98894afa7045"; name: string = "basic/3-fuji-xe3.mov" }                      |
| `basic/IMG_20160401_202342.jpg`           | object | { hash: string = "508324e9f99a6230c3f1835d9affcda49ad2861847c68f2787ef9e0560055b54"; name: string = "basic/IMG_20160401_202342.jpg" }             |
| `basic/P4260037.AVI`                      | object | { hash: string = "716460cf7ed500d999c005c31c006052102776f009f9a0e6ea12ad30d868d7b8"; name: string = "basic/P4260037.AVI" }                        |
| `basic/VID_20170727_142829.mp4`           | object | { hash: string = "193347216614d0100c6aef95e0c01cb1f6579946e176ffbd2f9afb030db80068"; name: string = "basic/VID_20170727_142829.mp4" }             |
| `basic/img_0119.jpg`                      | object | { hash: string = "62babedcaf47da185c4e57aace51c57fe13f0dd915e4ea9ed8021e5598fe5f0e"; name: string = "basic/img_0119.jpg" }                        |
| `basic/no-xmp-identifier.jpg`             | object | { hash: string = "411d17b51fe214d284f9242a606e8b57b1c4ca503d66778ab2c1f5fefe814baa"; name: string = "basic/no-xmp-identifier.jpg" }               |
| `basic/p2008-invalid.avi`                 | object | { hash: string = "8515cd068134405d1f4efc5da66b228c4e61aacdbaf3117626edf4ab929ce301"; name: string = "basic/p2008-invalid.avi" }                   |
| `basic/with-xmp-identifier.jpg`           | object | { hash: string = "2ef9ed4ab94ce85fa0dae21ac584295b5459878881bd54ac03463fc135b69040"; name: string = "basic/with-xmp-identifier.jpg" }             |
