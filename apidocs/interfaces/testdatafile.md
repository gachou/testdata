**[@gachou/testdata](../README.md)**

> [Globals](../README.md) / TestDataFile

# Interface: TestDataFile

## Hierarchy

- **TestDataFile**

## Index

### Properties

- [hash](testdatafile.md#hash)
- [name](testdatafile.md#name)

## Properties

### hash

• **hash**: string

_Defined in src/generated/index.ts:5_

---

### name

• **name**: string

_Defined in src/generated/index.ts:4_
