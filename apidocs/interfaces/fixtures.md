**[@gachou/testdata](../README.md)**

> [Globals](../README.md) / Fixtures

# Interface: Fixtures

## Hierarchy

- **Fixtures**

## Index

### Properties

- [basic/0-novideo.mp4](fixtures.md#basic/0-novideo.mp4)
- [basic/00000.MTS](fixtures.md#basic/00000.mts)
- [basic/1-video-streamable.mp4](fixtures.md#basic/1-video-streamable.mp4)
- [basic/123.mp4](fixtures.md#basic/123.mp4)
- [basic/2-video-unstreamable.mp4](fixtures.md#basic/2-video-unstreamable.mp4)
- [basic/2003-04-23\_\_15-49-58-img_0119.jpg](fixtures.md#basic/2003-04-23__15-49-58-img_0119.jpg)
- [basic/2015-08-19_P1010301.JPG](fixtures.md#basic/2015-08-19_p1010301.jpg)
- [basic/2016-08-02\_\_11-00-53-p1050073.jpg](fixtures.md#basic/2016-08-02__11-00-53-p1050073.jpg)
- [basic/3-fuji-xe3.mov](fixtures.md#basic/3-fuji-xe3.mov)
- [basic/IMG_20160401_202342.jpg](fixtures.md#basic/img_20160401_202342.jpg)
- [basic/P4260037.AVI](fixtures.md#basic/p4260037.avi)
- [basic/VID_20170727_142829.mp4](fixtures.md#basic/vid_20170727_142829.mp4)
- [basic/img_0119.jpg](fixtures.md#basic/img_0119.jpg)
- [basic/no-xmp-identifier.jpg](fixtures.md#basic/no-xmp-identifier.jpg)
- [basic/p2008-invalid.avi](fixtures.md#basic/p2008-invalid.avi)
- [basic/with-xmp-identifier.jpg](fixtures.md#basic/with-xmp-identifier.jpg)

## Properties

### basic/0-novideo.mp4

• **basic/0-novideo.mp4**: [TestDataFile](testdatafile.md)

_Defined in src/generated/index.ts:8_

---

### basic/00000.MTS

• **basic/00000.MTS**: [TestDataFile](testdatafile.md)

_Defined in src/generated/index.ts:9_

---

### basic/1-video-streamable.mp4

• **basic/1-video-streamable.mp4**: [TestDataFile](testdatafile.md)

_Defined in src/generated/index.ts:10_

---

### basic/123.mp4

• **basic/123.mp4**: [TestDataFile](testdatafile.md)

_Defined in src/generated/index.ts:11_

---

### basic/2-video-unstreamable.mp4

• **basic/2-video-unstreamable.mp4**: [TestDataFile](testdatafile.md)

_Defined in src/generated/index.ts:12_

---

### basic/2003-04-23\_\_15-49-58-img_0119.jpg

• **basic/2003-04-23\_\_15-49-58-img_0119.jpg**:
[TestDataFile](testdatafile.md)

_Defined in src/generated/index.ts:13_

---

### basic/2015-08-19_P1010301.JPG

• **basic/2015-08-19_P1010301.JPG**: [TestDataFile](testdatafile.md)

_Defined in src/generated/index.ts:14_

---

### basic/2016-08-02\_\_11-00-53-p1050073.jpg

• **basic/2016-08-02\_\_11-00-53-p1050073.jpg**:
[TestDataFile](testdatafile.md)

_Defined in src/generated/index.ts:15_

---

### basic/3-fuji-xe3.mov

• **basic/3-fuji-xe3.mov**: [TestDataFile](testdatafile.md)

_Defined in src/generated/index.ts:16_

---

### basic/IMG_20160401_202342.jpg

• **basic/IMG_20160401_202342.jpg**: [TestDataFile](testdatafile.md)

_Defined in src/generated/index.ts:17_

---

### basic/P4260037.AVI

• **basic/P4260037.AVI**: [TestDataFile](testdatafile.md)

_Defined in src/generated/index.ts:18_

---

### basic/VID_20170727_142829.mp4

• **basic/VID_20170727_142829.mp4**: [TestDataFile](testdatafile.md)

_Defined in src/generated/index.ts:19_

---

### basic/img_0119.jpg

• **basic/img_0119.jpg**: [TestDataFile](testdatafile.md)

_Defined in src/generated/index.ts:20_

---

### basic/no-xmp-identifier.jpg

• **basic/no-xmp-identifier.jpg**: [TestDataFile](testdatafile.md)

_Defined in src/generated/index.ts:21_

---

### basic/p2008-invalid.avi

• **basic/p2008-invalid.avi**: [TestDataFile](testdatafile.md)

_Defined in src/generated/index.ts:22_

---

### basic/with-xmp-identifier.jpg

• **basic/with-xmp-identifier.jpg**: [TestDataFile](testdatafile.md)

_Defined in src/generated/index.ts:23_
