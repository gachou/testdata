**[@gachou/testdata](../README.md)**

> [Globals](../README.md) / TestFileHandler

# Interface: TestFileHandler

## Hierarchy

- **TestFileHandler**

## Index

### Methods

- [andCopyTo](testfilehandler.md#andcopyto)

## Methods

### andCopyTo

▸ **andCopyTo**(...`path`: string[]): Promise\<string>

_Defined in src/index.ts:15_

#### Parameters:

| Name      | Type     |
| --------- | -------- |
| `...path` | string[] |

**Returns:** Promise\<string>
