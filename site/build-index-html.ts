import { files, TestDataFile } from "../src/generated";
import { html } from "./escape-html";
import { scss } from "./escape-css";

export function buildIndexHtml(): string {
	return (
		html`
			<!DOCTYPE html>
			<html>
				<head>
					<title>Gachou-Testdata</title>
					<style ref="stylesheet">
						 ${cssReset()}
						${styles()}
					</style>
				</head>
				<body>
					<nav>
						<h1>画帳 - Gachou<span class="secondary">-Testdata</span></h1>
					</nav>

					<table>
						<thead>
							<tr>
								<th>Name</th>
								<th>Hash</th>
							</tr>
						</thead>
						${Object.keys(files).map(createFileSection)}
					</table>
				</body>
			</html>
		`
			.toHtml()
			.trim() + "\n"
	);
}

function cssReset() {
	// see https://www.digitalocean.com/community/tutorials/css-minimal-css-reset
	return scss`
		html {
			box-sizing: border-box;
			font-size: 16px;
		}

		*,
		*:before,
		*:after {
			box-sizing: inherit;
		}

		body,
		h1,
		h2,
		h3,
		h4,
		h5,
		h6,
		p,
		ol,
		ul {
			margin: 0;
			padding: 0;
			font-weight: normal;
		}

		ol,
		ul {
			list-style: none;
		}

		img {
			max-width: 100%;
			height: auto;
		}
	`;
}

function styles() {
	return scss`

$accentColor: darken(#c77829,20%);
$textColor: #000;
$borderColor: #c77829;
$codeBgColor: #000;
$secondaryBgColor: #fff6d5;
$navHeight: 4rem;

body {
  padding: 1rem;
  margin: $navHeight 0 0 0;
}
nav {
  background-color: $secondaryBgColor;
  height: $navHeight;
  border-bottom: 1px solid $borderColor;
  position: fixed;
  left: 0;
  right: 0;
  top: 0;
  padding: 1rem;
  display: flex;
  box-sizing: border-box;

  h1 {
    padding: 0;
    margin: 0;
    font-size: 1.5rem;
    line-height: 2rem;

    .secondary {
      opacity: 0.5;
    }
  }
}

a {
  color: $accentColor;
}

table {
  background-color: white;
  border-collapse: collapse;
}

th {
  background-color: $secondaryBgColor;
}

td,th {
  border: 1px solid $borderColor;
  padding: 0.5rem;
  text-align: left;
}
`;
}

function createFileSection(fileName: string) {
	const file: TestDataFile = files[fileName];
	return html`
		<tr>
			<td><a href="files/${file.name}">${file.name}</a></td>
			<td>${file.hash}</td>
		</tr>
	`;
}
