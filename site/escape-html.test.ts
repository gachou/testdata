import { html, safe } from "./escape-html";

test("escapes substituted values", () => {
	expect(html`<b>${"<hr>"}</b>`.toHtml()).toEqual("<b>&lt;hr&gt;</b>");
});

test("does not escaped safe strings", () => {
	expect(html`<b>${safe("<hr>")}</b>`.toHtml()).toEqual("<b><hr></b>");
});

test('does not escape result of "html"-call', () => {
	expect(html`<b>${html`<hr />`}</b>`.toHtml()).toEqual("<b><hr /></b>");
});

test("joins arrays in a substitution", () => {
	expect(html`<b>${["a", "b", "c"]}</b>`.toHtml()).toEqual("<b>abc</b>");
});

test('does not escape result of "html" in array substitution', () => {
	expect(html` <b> ${["a", "b"].map((x) => html` <i>${x}</i> `)} </b>`.toHtml()).toEqual(
		" <b>  <i>a</i>  <i>b</i>  </b>"
	);
});
