import fs from 'fs-extra'
import path from 'path'
import { buildIndexHtml } from "./build-index-html";

const outputDirectory = 'public';

async function createSite() {

  await fs.remove(outputDirectory)
  await fs.mkdirp(outputDirectory)
  await fs.copy('testdata', path.join(outputDirectory,'files'))
  await fs.writeFile(path.join(outputDirectory, 'index.html'),buildIndexHtml());
}


// eslint-disable-next-line no-console
createSite().then(console.log, console.error)
