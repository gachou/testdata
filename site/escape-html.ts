interface SafeString {
	toHtml(): string;
}

const specialChars = /[&<>"'`=]/g;
const replacements = {
	"&": "&amp;",
	"<": "&lt;",
	">": "&gt;",
	'"': "&quot;",
	"'": "&#x27;",
	"`": "&#x60;",
	"=": "&#x3D;",
};

export function safe(value: string): SafeString {
	return {
		toHtml() {
			return value;
		},
	};
}

type Substitution = string | SafeString | Substitution[];

export function html(
	template: TemplateStringsArray,
	...substitutions: Substitution[]
): SafeString {
	const escapedSubstitutions = substitutions.map(htmlEscape);
	return {
		toHtml() {
			return String.raw(template, ...escapedSubstitutions);
		},
	};
}

function htmlEscape(value: Substitution): string {
	if (typeof (value as SafeString).toHtml == "function") {
		return (value as SafeString).toHtml();
	}
	if (Array.isArray(value)) {
	  return value.map(htmlEscape).join('')
  }
	return (value as string).replace(specialChars, (char) => replacements[char]);
}
