import sass from "node-sass";

export function scss(template: TemplateStringsArray, ...substitutions: string[]): string {
	const scssResult = String.raw(template, ...substitutions);
	return sass.renderSync({ data: scssResult, includePaths: ["."] }).css.toString("utf8");
}
